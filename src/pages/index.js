import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <h1>Howdy dude named ben!</h1>
    <p>Welcome to the NAlocal512 gitlab.</p>
    <p>
      Dudes named ben and dudettes named bernadette should join our <a href="https://gitlab.com/groups/nalocal512/-/group_members">gitlab team</a>.
    </p>
    <p>This is where we will collaborate on our <a href="https://NAlocal512.com/">web site</a> and other web adventures.</p>
    <p>Go to our repo <Link to="/repos/">page</Link> to find a listing of the top repos where most of our work will be done.</p>
    <p>Our websites are built in <a href="https://gatsbyjs.org/">Gatsby</a> a web framework built to make blazing fast static websites from a template framework that allows you to use react components.</p>
    <p>The fantastic CI/CD available here at <a href="https://gitlab.com/">Gitlab</a> reads the <code>.gitlab-ci.yml</code> performs a build and test of our site.  </p>
    <p>And then publishes the rendered static site to the CDN, which is proxied by <a href="https://cloudflare.com/">Cloudflare</a>, to give front-end sites that are wicked fast.</p>
    <p>Then any dynamic pieces that require a database backend or other server side processing, like the shop,  mailing list and domain management are conducted by the <a href="https://webhosting.coop/">Webhosting.coop</a>.</p>
    <p>If you would like to learn to become a dude named ben who helps with our web sites try starting on this <a href="https://NAlocal512.com/benify/">page</a> to find a listing of the top resources we think might help you get started.</p>
    <p></p>
    <p>Please feel free to fork our repos for your own projects, and if you feel something could be better we appreciate pull requests.</p>
    <p>Now go build something great.</p>
    <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
      <Image />
    </div>
  </Layout>
)

export default IndexPage
