import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

const RepoPage = () => (
  <Layout>
    <SEO title="Repo Page" />
    <h1>Hi from the repo page</h1>
    <p>Welcome to the repo page</p>
    <p>We have three main repos at the moment:</p>
    <p>Art Assets <a href="https://gitlab.com/nalocal512/artassets/">Repository</a> -- this is where the logo and any other art assets will live.</p>
    <p><a href="https://gitlab.com/nalocal512/nalocal512/">Repository</a> for the main NAlocal512 web site.</p>
    <p><a href="https://gitlab.com/nalocal512/nalocal512.gitlab.io/">Repository</a> for this web site that will point developers in the right direction to collaborate with us.</p>
    <p></p>
    <p>You can find a full list of our repos <a href="https://gitlab.com/nalocal512">here</a>.</p>
    <p></p>
    <Link to="/">Go back to the homepage</Link>
  </Layout>
)

export default RepoPage
