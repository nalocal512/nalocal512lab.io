module.exports = {
  siteMetadata: {
    title: `NAlocal512`,
    description: `This site will be the focal point for developers coming in to collaborate on our NAlocal512 web presence.`,
    author: `@gatsbyjs`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `NAlocal512.gitlab.io`,
        short_name: `NAlocal512`,
        start_url: `/`,
        background_color: `#663333`,
        theme_color: `#663333`,
        display: `minimal-ui`,
        icon: `src/images/NAlocal512.png`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
